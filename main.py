from typing import Optional
import json
from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()
import inference

category_classifier, word_segmenter = inference.load_classifier()

@app.get("/")
async def read_root():
    return {"Hello": "World"}

class ItemInput(BaseModel):
    name: str

@app.post("/item-classification/inference/")
async def create_item(item: ItemInput):
    result = inference.predict(item.name, category_classifier, word_segmenter)
    return result