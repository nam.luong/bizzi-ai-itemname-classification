from libs import *
from config import *

def preprocess_sentence(sent: str, word_segmenter) -> str:
    sent = sent.lower().strip()
    sent = re.sub(r"\s{2,}", " ", sent)  # Remove redundant spaces
    sent = " ".join(word_segmenter.tokenize(sent)[0])
    return sent

def predict(item_name: str, category_classifier, word_segmenter) -> Dict[str, str]:
    preprocessed_item_name = preprocess_sentence(item_name, word_segmenter)
    prediction = category_classifier(preprocessed_item_name)[0]
    prediction["itemName"] = item_name
    return prediction

def load_classifier():
    model = AutoModelForSequenceClassification.from_pretrained("model_weights/2020-3-29-pretrained-vncorenlp-no-vocab-added-phase-2")

    tokenizer = AutoTokenizer.from_pretrained("model_weights/tokenizer")
    word_segmenter = VnCoreNLP(
            "model_weights/vncorenlp/VnCoreNLP-1.1.1.jar",
            annotators="wseg",
            max_heap_size="-Xmx500m",
        )
    category_classifier = pipeline(
        "text-classification", model=model, tokenizer=tokenizer
    )
    return category_classifier, word_segmenter

if __name__ == "__main__":
    category_classifier, word_segmenter = load_classifier()
